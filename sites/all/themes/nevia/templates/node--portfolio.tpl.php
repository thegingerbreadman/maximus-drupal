<?php
if ($_POST) { 
  $ToEmail = variable_get('site_mail', '');; 
  $EmailSubject = 'Site contact form'; 
  //$mailheader = "From: ".$_POST["email"]."\r\n";
  $mailheader = "Content-type: text/html; charset=iso-8859-1\r\n"; 
  $MESSAGE_BODY = "<br><b>Name:</b> ".$_POST["name"]."";
  $MESSAGE_BODY .= "<br><b>Email:</b> ".$_POST["email"]."";
  if($_POST["item"]) $MESSAGE_BODY .= "<br><b>Item:</b> ".$_POST["item"].""; 
  $MESSAGE_BODY .= "<br><b>Message:</b><br>".nl2br($_POST["message"])."<br>"; 
  mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure"); 
}?>

<script>
jQuery(document).ready(function() {
  jQuery('#form-submit').click(function() {
    var nam = jQuery('#field-name').val();
    var eml = jQuery('#field-email').val();
    var itm = window.location.pathname;
    var msg = jQuery('#field-message').val();

    jQuery('.form-error').removeClass('form-error');

    if (!nam.trim()) {
      jQuery('#field-name').addClass('form-error');
      return false;
    }

    if (!eml.trim()) {
      jQuery('#field-email').addClass('form-error');
      return false;
    }

    if (!msg.trim()) {
      jQuery('#field-message').addClass('form-error');
      return false;
    }

    jQuery.ajax({
      type: "POST",
      url: window.location.pathname,
      data: {
          name: name,
          email: eml,
          item: itm,
          message: msg
      }
    });
    return false;
  });

  jQuery('.portfolio-img').click(function() {
    jQuery('#featured-img').html(jQuery(this).html());
  });
});

</script>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> portfolio-content clearfix"<?php print $attributes; ?>>

  <?php if (!empty($content['uc_product_image']) && $page): ?>
    <div class="five columns omega">
    <?php endif; ?>
    <?php $content_class = ''; ?>
    <?php if ($display_submitted): ?>
      <?php $content_class = ' display-submitted'; ?>
      <div class="submitted">
        <section class="date">
          <span class="day"><?php print format_date($node->created, 'custom', 'd'); ?></span>
          <span class="month"><?php print format_date($node->created, 'custom', 'M'); ?></span>
        </section>

      </div>
    <?php endif; ?>

    <div class="content<?php print $content_class; ?> container"<?php print $content_attributes; ?>>

      <?php if ($title || $submitted): ?>
        <header class="meta">
          <?php print render($title_prefix); ?>
          <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php if ($page && ($node->type == 'product')): ?>
            <h3<?php print $title_attributes; ?>><?php print $title; ?></h3>
          <?php endif; ?>
          <?php print render($title_suffix); ?>

          <?php if ($display_submitted): ?>
            <?php print $submitted; ?>
          <?php endif; ?>
        </header>
      <?php endif; ?>

      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      if (!empty($content['field_tags'])) {
        hide($content['field_tags']);
      }

      /*
      * Render content! :)
      */
      //print render($content);
      if (!empty($content['field_image'])): ?>
      <div class="portfolio-img-wrapper eight columns">
      <div id="featured-img" class="mobile-hide"><?php print render($content['field_image'][0]);?></div>
      <div class="portfolio-img-thumbs">
          <?php 
          $size = count($content['field_image']);
          for ($i=0; $i<$size; $i++) {
            if (!empty($content['field_image'][$i])) {
              $pos = '';
              if(($i+4)%4 == 0){$pos = 'alpha';}
              if(($i+1)%4 == 0){$pos = 'omega';}
              print "<div class='portfolio-img two columns " . $pos . "'>";
              print render($content['field_image'][$i]);
              print "</div>";
            }
          }?>
      </div></div>
      <?php endif; ?>

      <div class="portfolio-text-wrapper seven columns">
        <h2><?php print render($content['field_heading']); ?></h2>
        <h3><?php print render($content['field_subtitle']); ?></h3>
        <p><?php print render($content['body']); ?></p>
        <p><?php print render($content['field_helpful_information']); ?></p>
        <h3>Ask a question:</h3>
        <form id="form-fields" method="post">
          <input type="text" name="name" id="field-name" class="alpha seven columns" placeholder="NAME">
          <input type="text" name="email" id="field-email" class="alpha seven columns" placeholder="EMAIL">
          <textarea name="message" id="field-message" class="alpha seven columns" placeholder="MESSAGE" rows="8"></textarea>
          <div class="seven columns"><input type="submit" value="Enquire about this item" id="form-submit"></div>
        </form>
      </div>

      <?php if (!$page) print '<a class="button color" href="' . url('node/' . $node->nid) . '">' . t('Read More') . '</a>';?>
      <?php if ($page) print render($content['links']);?>
    </div>

    <?php if (!empty($content['uc_product_image']) && $page): ?>
    </div>
  <?php endif; ?>

</div>
