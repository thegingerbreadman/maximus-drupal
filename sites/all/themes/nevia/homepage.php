<?php
$vid = 2; //VID of 'Catalog'
$taxonomies = taxonomy_get_tree($vid);
$portfoliopath = url("portfolio/portfolio");
$page = '';
//$page = drupal_get_path_alias($_GET['q']);

function rowCount($i,$page) {
  //if(strpos($page,'blog') !== false) return;
  //if($i==1){return 'alpha';}
  if($i==3){return 'omega';}
}

function convertName($name) {
  $n = strtolower($name);
  $n = preg_replace('/[^a-zA-Z0-9\']+/', '-', $n);
  return $n;
}
?>

<!--products block-->
<div class="content products-wrapper">
<?php 
$i = 0;
$terms = array();
foreach ($taxonomies as $cat) {
  if ($cat->parents[0] == 0) {//filter out sub categories
  $name = convertName($cat->name);
?>
  <a href="<?echo $portfoliopath."#".$name?>" data-option-value="<?echo ".".$name?>"><div class="grey-button button-row one-third column <?if($i++>=3)$i=1;echo rowCount($i,$page);?>">
    <span><?echo $cat->name;?></span>
  </div></a>
<?}}?>
</div>
<!--end products block-->