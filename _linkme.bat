@ECHO OFF
echo Requires RUNAS ADMINISTRATOR
echo y|rmdir c:\wamp\www
for /f %%i in ("%0") do set curpath=%%~dpi 
cd /d %curpath% 
mklink /d c:\wamp\www %curpath% 
echo Remember to enable:
echo 1. php_curl
echo 2. php short open tags
echo #. apache rewrite engine
PAUSE